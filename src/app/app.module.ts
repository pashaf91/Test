import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenuListComponent } from './menu/menu-list/menu-list.component';
import { MenuDetailComponent } from './menu/menu-detail/menu-detail.component';
import { CartComponent } from './cart/cart.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuService } from './menu/menu.service';
import { CartListService } from './cart/cart-list.service';
import { MenuItemComponent } from './menu/menu-item/menu-item.component';
import { MenuComponent } from './menu/menu.component';
import { DropdownDirective } from './directives/dropdown.directive';
import { CategoryPipe } from './menu/filter/category.pipe';
import { FormComponent } from './menu/form/form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuListComponent,
    MenuDetailComponent,
    CartComponent,
    MenuItemComponent,
    MenuComponent,
    DropdownDirective,
    CategoryPipe,
    FormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [MenuService, CartListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
