import {Directive, ElementRef, HostBinding, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appDpordown]'
})
export class DropdownDirective {

  @HostBinding('class.open') isOpen = false;

  constructor(private render: Renderer2, elem: ElementRef) {
  }

  @HostListener('click')
  toggle(event: Event) {
    this.isOpen = !this.isOpen;
  }

}