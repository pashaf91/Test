import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuDetailComponent } from './menu/menu-detail/menu-detail.component';
import { CartComponent } from './cart/cart.component';
import { MenuComponent } from './menu/menu.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/menu', pathMatch: 'full'},
  {path: 'menu', component: MenuComponent,
    children: [
      {path: ':id', component: MenuDetailComponent},
    ]
  },
  {path: 'cart', component: CartComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
