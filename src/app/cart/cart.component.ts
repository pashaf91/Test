import { Component, OnDestroy, OnInit } from '@angular/core';
import { CartListService } from './cart-list.service';
import { Menu } from '../menu/menu.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {
  menuItems: Menu[];
  menuItemIndex: number;
  private subscription: Subscription;


  constructor(private clService: CartListService) {
  }

  ngOnInit() {
    this.menuItems = this.clService.getMenuItems();
    this.subscription = this.clService.newMenuItem
      .subscribe((item: Menu) => {
        this.menuItems.push(item);
      });

    this.clService.menuChanged
      .subscribe((res) => {
        this.menuItems = res;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onDelete(index: number) {
    console.log(this.menuItemIndex);
    this.clService.deleteMenuItem(index);
  }

  getTotal() {
    let total = 0;
    for (let i = 0; i < this.menuItems.length; i++) {
      if (this.menuItems[i].price) {
        total += this.menuItems[i].price;
      }
    }
    return total;
  }

}
