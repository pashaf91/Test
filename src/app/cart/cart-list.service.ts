import { Injectable } from '@angular/core';
import { Menu } from '../menu/menu.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CartListService {
  private menuItems: Menu[] = [
    new Menu(Date.now() + 1, 'Pasta', '', 20, 'meat'),
    new Menu(Date.now() + 2, 'Icecream', '', 35, 'cake'),
  ];
  /**
   * create new Subject to pass new ingredient object to other components
   * @type {Subject<Menu>}
   */
  newMenuItem = new Subject<Menu>();
  menuChanged = new Subject<Menu[]>();

  constructor() {
  }

  /**
   * create a copy of menuItems array (with slice & no parameters) &
   * make it public
   * @returns {Menu[]}
   */
  getMenuItems() {
    return this.menuItems.slice();
  }

  getMenuItem(index: number) {
    return this.menuItems[index];
  }

  addMenuItem(menuItem: Menu) {
    if (!this.menuItems.includes(menuItem)) {
      this.menuItems.push(menuItem);
    }
  }

  deleteMenuItem(index: number) {
    this.menuItems.splice(index, 1);
    this.menuChanged.next(this.menuItems.slice());
  }
}
