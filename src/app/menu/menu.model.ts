export class Menu {
  public id: any;
  public name: string;
  public imagePath: string;
  public price: number;
  public category: string;

  constructor(id: any, name: string, imagePath: string, price: number, category: string) {
    this.id = id;
    this.name = name;
    this.imagePath = imagePath;
    this.price = price;
    this.category = category;
  }
}
