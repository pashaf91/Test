import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Menu } from '../menu.model';

@Pipe({
  name: 'categoryFilter',
  pure: false
})
@Injectable()
export class CategoryPipe implements PipeTransform {
  transform(menuItems: Menu[], args: any[]): any {
    let items = menuItems.filter(menuItems => menuItems.category.toLowerCase().indexOf(args[0].category.toLowerCase()) !== -1);
    return items;
  }
}
