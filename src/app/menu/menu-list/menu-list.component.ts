import { Component, OnInit } from '@angular/core';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {
  menuItems;
  filterItems;
  filterArgs = [{category: ''}];

  constructor(private mList: MenuService) {
  }

  ngOnInit() {
    this.menuItems = this.mList.getMenusItems();
    this.filterItems = this.mList.getFilter();
  }

  onFilter(name: string) {
    this.filterArgs = [{category: name}];
  }

}
