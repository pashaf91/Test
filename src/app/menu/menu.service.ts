import { Injectable, OnInit } from '@angular/core';

import { Menu } from './menu.model';
import { CartListService } from '../cart/cart-list.service';
import { Filter } from './filter.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MenuService {
  updatedMenu: Subject<any> = new Subject;
  private menu: Menu[] = [
    new Menu(Date.now() + 1, 'Burger',
      'http://bk-emea-prd.s3.amazonaws.com/sites/burgerking.co.uk/files/menu-categories/Burgers.jpg',
      15,
      'meat'
    ),
    new Menu(Date.now() + 2, 'Meat & fries',
      'https://www.savoredjourneys.com/wp-content/uploads/2015/10/schnitzel-germany.jpg',
      20,
      'meat'
    ),
    new Menu(Date.now() + 3, 'Cake',
      'https://i.pinimg.com/736x/b4/01/47/b40147865378fca69dfcdc883f7eeb4c--th-birthday-kids-birthday-cake-girls.jpg',
      40,
      'cake'
    )
  ];

  private filter: Filter[] = [
    new Filter('Meat'),
    new Filter('Cake')
  ];

  constructor(private clService: CartListService) {
  }

  getMenusItems() {
    return this.menu;
  }

  getFilter() {
    return this.filter.slice();
  }

  getMenuItem(id: number) {
    for (let item of this.menu) {
      if (item.id === id) {
        return item;
      }
    }

  }

  addNewMenuItem(item: any) {
    console.log(item);
    if (!this.menu.includes(item)) {
      this.menu.push(item);
    }
    console.log(this.menu);
  }

  addIngredientsToCart(ingredients: Menu) {
    this.clService.addMenuItem(ingredients);
  }

}
