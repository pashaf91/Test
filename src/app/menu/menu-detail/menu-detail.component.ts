import { Params, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../menu.service';
import { Menu } from '../menu.model';

@Component({
  selector: 'app-menu-detail',
  templateUrl: './menu-detail.component.html',
  styleUrls: ['./menu-detail.component.css']
})
export class MenuDetailComponent implements OnInit {
  id: number;
  menu: Menu;

  constructor(private mlService: MenuService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.menu = this.mlService.getMenuItem(this.id);
        }
      );
  }

  onAddToCart() {
    this.mlService.addIngredientsToCart(this.menu);
  }

}
